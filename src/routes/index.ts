import { Router, Request, Response } from 'express';

export const routes = Router();

routes.get("/", (req: Request, res: Response): void => {
    res.render("index", { 
        title: "title", 
        description: "desc",
        cannonical: "/"
     });
});